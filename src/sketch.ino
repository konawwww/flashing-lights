#define GREEN_LED_PIN 11
#define RED_LED_PIN 12
#define BUTTON_PIN 2
#define FREQUENCY 500

void setup()
{
  pinMode(RED_LED_PIN, OUTPUT);
  pinMode(GREEN_LED_PIN, OUTPUT);
}

int leds[] = {GREEN_LED_PIN, RED_LED_PIN};
int current_led = 0;
int switch_status = LOW;
int current_status;

void loop()
{
  current_led = !current_led;
  current_status = switch_status;

  while (digitalRead(BUTTON_PIN) == HIGH) {
    if (switch_status == current_status && switch_status == LOW) {
      switch_status = HIGH;
    }

    if (switch_status == current_status && switch_status == HIGH) {
      switch_status = LOW;
    }

    delay(100);
  }

  digitalWrite(leds[!current_led], LOW);
  digitalWrite(leds[current_led], switch_status);
  delay(FREQUENCY);
}
