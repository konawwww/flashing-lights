#include <Arduino.h>

void setup();
void loop();
#line 1 "src/sketch.ino"
#define GREEN_LED_PIN 10
#define RED_LED_PIN 11

void setup()
{
  pinMode(RED_LED_PIN, OUTPUT);
  pinMode(GREEN_LED_PIN, OUTPUT);
}

int leds[] = {GREEN_LED_PIN, RED_LED_PIN};
int current_led = 0;
int pot_setting;
int frequency;

void loop()
{
  current_led = !current_led;
  pot_setting = analogRead(A0);
  frequency = pot_setting;

  digitalWrite(leds[!current_led], LOW);
  digitalWrite(leds[current_led], HIGH);
  delay(frequency);
}
